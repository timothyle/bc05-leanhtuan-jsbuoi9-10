function kiemTraRong(userInput, idErr){
  if(userInput.length == 0){
   showMessageErr(idErr, "Không được để rỗng");
   return false
  } else {
   showMessageErr(idErr, "");
   return true
  }
}

function kiemTraTrung(id,dsnv){
   let index = timKiemViTri(id,dsnv)
   if(index !== -1){
       showMessageErr("tbTKNV", "Mã nhân viên này đã tồn tại")
       return false;
   }else{
       showMessageErr("tbTKNV","")
       return true;
   }
}
function kiemTraChucVu(vtcv,idErr){
  if(vtcv == "Chọn chức vụ"){
      showMessageErr(idErr, "Vui lòng không để rỗng")
      return false;
  }else{
      showMessageErr(idErr,"")
      return true;
  }
}

function kiemTraLuong(value, idErr){
  var reg = /^(100000\d|10000[1-9]\d|1000[1-9]\d{2}|100[1-9]\d{3}|10[1-9]\d{4}|1[1-9]\d{5}|[2-9]\d{6}|1\d{7}|20000000)$/;
  let isLuong = reg.test(value);
  if (isLuong){
    showMessageErr(idErr,"");
    return true;
  } else {
    showMessageErr(idErr,"Lương phải là số và từ 1,000,000 tới 20,000,000");
    return false;
  }
}
function kiemTraGioLam(value, idErr){
  var reg = /^(8[0-9]|9[0-9]|1[0-9]{2}|200)$/;
  let isGioLam = reg.test(value);
  if (isGioLam){
    showMessageErr(idErr,"");
    return true;
  } else {
    showMessageErr(idErr,"Giờ làm từ 80 tới 200");
    return false;
  }
}

function kiemTraSo(value, idErr){
    var reg = /^[0-9]{4,6}$/;
    let isNumber = reg.test(value);
    if (isNumber){
      showMessageErr(idErr," ");
      return true;
    } else {
      showMessageErr(idErr,"Mã nhân viên phải là số và ít hơn 6 chữ số");
      return false;
    }
  }
  
  function kiemTraEmail(value, idErr){
    var reg =  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
    let isEmail = reg.test(value);
  
    if (isEmail){
      showMessageErr(idErr,"");
      return true;
    } else {
      showMessageErr(idErr,"Email không đúng định dạng");
      return false;
    }
  }

function kiemTraChu(value,idErr){
  var reg =/^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
  let isText = reg.test(value);
  
  if (isText){
    showMessageErr(idErr,"");
    return true;
  } else {
    showMessageErr(idErr,"Tên không đúng định dạng");
    return false;
  }
}

function kiemTraMatKhau(value,idErr){
  var reg = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
  let isPassword = reg.test(value);
  
  if (isPassword){
    showMessageErr(idErr,"");
    return true;
  } else {
    showMessageErr(idErr,"Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)");
    return false;
  }
}
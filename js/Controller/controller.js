function hienThongTinNhanVien() {
    var taikhoan = document.getElementById("tknv").value;
    var tenNV = document.getElementById("name").value;
    var email1 = document.getElementById("email").value;
    var matKhau =  document.getElementById("password").value;
    var ngayLam = document.getElementById("datepicker").value;
    var luongGoc =  document.getElementById("luongCoBan").value;
    var viTriChucVu = document.getElementById("chucvu").value;
    var sogioLam =  document.getElementById("giodiLam").value;
    var NV = new NhanVien(taikhoan,tenNV,email1,matKhau,ngayLam,luongGoc,viTriChucVu,sogioLam);
    return NV;
}

function renderDSNV(danhsach) {
    var contentHTML = "";
    for (var index = 0; index < danhsach.length; index++) {
    var currentNV = danhsach[index];

    var contentTr = `
    <tr>
       <td>${currentNV.maNV}</td>
       <td>${currentNV.ten}</td>
       <td>${currentNV.email}</td>
       <td>${currentNV.ngayLamform}</td>
       <td>${currentNV.vtcv}</td>
       <td>${currentNV.tongluong}</td>
       <td id="XepLoai">${currentNV.loaiNV}</td>
       <td>
       <button onclick="xoaNV('${currentNV.maNV}')" class="btn btn-danger">Xóa</button></td>
       <td><button data-toggle="modal" data-target="#myModal" class='btn btn-success' onclick="suaNV(${currentNV.maNV})">Sửa</button>
       </td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(id,danhsach) {
  for (var index=0; index < danhsach.length; index++) {
    var item = danhsach[index];
    //neu tim thay thi duwngf function va tra ve index hien tai
    if(item.maNV==id){
      return index;
    }
  }
  //quydinh neu khong tim thay thi tra ve -1
  return -1;
}

function showThongTinLenForm(NV){
  document.getElementById("tknv").value = NV.maNV;
  document.getElementById("name").value = NV.ten;
  document.getElementById("email").value = NV.email;
  document.getElementById("password").value = NV.matKhauform;
  document.getElementById("datepicker").value = NV.ngayLamform;
  document.getElementById("luongCoBan").value = NV.luongCB;
  document.getElementById("chucvu").value = NV.vtcv;
  document.getElementById("giodiLam").value = NV.gioLam;
}
function resetForm(){
  document.getElementById("form-modal-input").reset();
}
function showMessageErr(idErr,message){
  document.getElementById(idErr).innerHTML = message;
}

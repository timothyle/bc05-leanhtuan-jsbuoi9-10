class NhanVien{
    constructor(taikhoan, tenNV, email1, matKhau, ngayLam, luongGoc, viTriChucVu, sogioLam) {
    this.maNV = taikhoan;
    this.ten = tenNV;
    this.email = email1;
    this.matKhauform = matKhau;
    this.ngayLamform = ngayLam;
    this.luongCB = luongGoc;
    this.vtcv = viTriChucVu;
    this.gioLam = sogioLam;
    
    this.tongluong = this.vtcv === 'Giám đốc' ? this.luongCB * 3
    : this.vtcv === 'Trưởng phòng' ? this.luongCB * 2 : this.luongCB

    this.loaiNV = this.gioLam >= 192 ? 'Xuất sắc'
    : this.gioLam >= 176 ? 'Giỏi'
        : this.gioLam >= 160 ? 'Khá' : 'Trung bình'

}}

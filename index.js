const DSNV = "DSNV";
var dsnv = [];

function luuLocalStorage(){
  let jsonDSNV = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, jsonDSNV);
}

var dataJson = localStorage.getItem(DSNV);
console.log("dataJson:", dataJson);
if (dataJson !== null){
  var danhsach = JSON.parse(dataJson);

  for (var index=0; index <danhsach.length; index++){
    var item = danhsach[index];
    var NV = new NhanVien(
      item.maNV,
      item.ten,
      item.email,
      item.matKhauform,
      item.ngayLamform,
      item.luongCB,
      item.vtcv,
      item.gioLam,
      item.tongluong,
      item.loaiNV,
    )
    dsnv.push(NV);
  }
  renderDSNV(dsnv);
}

function btnThemNV(NV) {
    var NV = hienThongTinNhanVien();
    var isValid = true;

    isValid &= kiemTraRong(NV.maNV, "tbTKNV") 
    && kiemTraSo(NV.maNV, "tbTKNV") 
    && kiemTraTrung(NV.maNV, dsnv);

    isValid &= kiemTraRong(NV.ten, "tbTen") && kiemTraChu (NV.ten, "tbTen");

    isValid &= kiemTraRong(NV.email, "tbEmail") && kiemTraEmail(NV.email, "tbEmail");

    isValid &= kiemTraRong(NV.matKhauform, "tbMatKhau") && kiemTraMatKhau(NV.matKhauform, "tbMatKhau");

    isValid &= kiemTraRong(NV.ngayLamform, "tbNgay");

    isValid &= kiemTraRong(NV.luongCB, "tbLuongCoBan") && kiemTraLuong(NV.luongCB, "tbLuongCoBan");

    isValid &= kiemTraChucVu(NV.vtcv, "tbChucVu");

    isValid &= kiemTraRong(NV.gioLam, "tbGioDilam") && kiemTraGioLam(NV.gioLam, "tbGioDilam");

    if (isValid){
    dsnv.push(NV);
    // add vào array
    luuLocalStorage();
    // render danh sách sinh viên
    renderDSNV(dsnv);
    }
}
function xoaNV(id){
  console.log("id: ", id)
  //splice
  var viTri = timKiemViTri(id,dsnv);
  console.log("viTri: ", viTri);
  if (viTri!== -1)
  {
    dsnv.splice(viTri,1);
    luuLocalStorage();
    //render lai giao dien sau khi xoa
    renderDSNV(dsnv);
  }
} 

function suaNV(id){
  var viTri = timKiemViTri(id,dsnv);
  if (viTri == -1) return;
  var data = dsnv[viTri];
  showThongTinLenForm(data);
  luuLocalStorage();
  //nga can user edit id sv
  document.getElementById("tknv").disabled = true;
  document.getElementById("btnThem").disabled = true;
}

function capNhatNV(){
  var data = hienThongTinNhanVien();
  console.log("data:",data);
  var viTri = timKiemViTri(data.maNV,dsnv);
  if (viTri == -1) return;

  dsnv[viTri] = data;
  renderDSNV(dsnv);
  luuLocalStorage();
  //remove disable
  document.getElementById("tknv").disabled = false;
  resetForm();
}

function timNVloaiNV(loaiNV){
  let dsLoaiNV = [];
  dsnv.map((NV)=>{
             if(NV.loaiNV.toLowerCase() === loaiNV)
                dsLoaiNV.push(NV)
        })
        console.log(dsLoaiNV)
        renderDSNV(dsLoaiNV);
}



function timKiemNhanVien(){
  let loai = document.getElementById("searchName").value.toLowerCase(); 
  console.log("loai",loai);
  if (loai.trim() !== "") {
    if (loai == 'xuất sắc' || loai == 'giỏi' || loai == 'khá' || loai == 'trung bình') {
      hienThongTinNhanVien(timNVloaiNV(loai))
  } else
      document.getElementById("tbTimKiem").innerHTML = 'Bạn chỉ có thể tìm nhân viên xuất sắc, giỏi, khá hay trung bình'
} else
document.getElementById("tbTimKiem").innerHTML = 'Chưa nhập loại nhân viên'
}

